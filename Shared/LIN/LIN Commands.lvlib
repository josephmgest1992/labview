﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Description" Type="Str">10/28/2016
Added GetModuleConfig
Added ControlWirePatternDisplay
Added ModId inputs to all previous vi's</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)J!!!*Q(C=\&gt;3^&lt;?.!%-8R:]0"!9L5Q5%N4!NK13V-#UI&gt;KI0$K]#!'H#A#A[9\')F,I!NW(_OZAYW9*O!=6_"FR[*?EPO`LR=5"LNCX3DR[8W`&gt;XWP(]V;H5Z?&lt;N`N&gt;!`8`'R]=@1&lt;`=P_6`P8WI`XGW@`:`^`\:`I@%?O&amp;X?Z(_W36?;8U255;;3#MV&gt;DU6?Z%6?Z%6?Z#9XO=F.&lt;H+4*XG3*XG3*XG3"XG1"XG1"XG1DY.=Z#)8/;N34&amp;Z-6!R;$&amp;"=$%8&amp;2_%J0)7H]0"4B;@Q&amp;*\#5XCY2)7H]"3?QF.YO%W&amp;J`!5HM*4?"BK3'I=Z(A+$]0,?)T(?)T(?*B3RG-!:D)TM"E%BESH/4%?YT%?4G5]RG-]RG-]&gt;-NYD-&gt;YD-&gt;YO'7MCI&gt;G0MDR-)Q34_**0)EH]4#U%E`C34S**`%QH2*0YEE1S94*Y"#5X*2=E0R)0)G(,S7?R*.Y%E`CI7M]I2QL-WPGARR0Y!E]A3@Q""['5/!*0)%H]!1?BF8A#4S"*`!%(K:3Y!E]A3?!"*-SP9,"ABO$CY)A]0!X&gt;EO-J_1BC8&amp;)P8H6GV+^W&gt;3&lt;3,UZV!^&gt;`4$6$UG^_/J&amp;63_7?B(5`ZQ;L=;I*V(@0&amp;_IC=]T&gt;;+/V)(;5TNK3WWI^8TL&lt;\ZQGC;&gt;TW?&gt;4C=&gt;DU=&gt;$A@N^XPN&gt;DNNNVNN.BONVWP^&lt;&amp;]Z,OXKR8NJOK?_58?8]Y@\P`2?_B`?D&lt;L7Z@P8'DU"L8]NV1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.1.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI_IconEditor" Type="Str">49 53 48 48 56 48 50 57 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 10 1 100 1 100 80 84 72 48 0 0 0 36 0 1 0 3 2 86 73 9 51 114 100 32 112 97 114 116 121 18 77 121 84 101 109 112 108 97 116 101 95 76 73 78 46 112 110 103 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 255 197 0 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 255 197 0 255 197 0 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 255 197 0 0 127 127 255 197 0 255 197 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 255 197 0 0 127 127 255 197 0 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 255 197 0 255 197 0 255 197 0 0 127 127 255 197 0 0 127 127 255 197 0 0 127 127 0 127 127 255 197 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 127 127 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 4 200 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 212 66 0 255 212 66 0 255 197 0 0 255 254 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="5Wire Undercover" Type="Folder">
		<Item Name="mPowerUc_GetColorId.vi" Type="VI" URL="../5Wire Undercover/mPowerUc_GetColorId.vi"/>
		<Item Name="mPowerUc_SetColorId.vi" Type="VI" URL="../5Wire Undercover/mPowerUc_SetColorId.vi"/>
	</Item>
	<Item Name="_NET Bootload Commands" Type="Folder">
		<Item Name="ApplicationVerify.vi" Type="VI" URL="../_NET Bootload Commands/ApplicationVerify.vi"/>
		<Item Name="CloseRef.vi" Type="VI" URL="../_NET Bootload Commands/CloseRef.vi"/>
		<Item Name="EraseFlash.vi" Type="VI" URL="../_NET Bootload Commands/EraseFlash.vi"/>
		<Item Name="FlashFill.vi" Type="VI" URL="../_NET Bootload Commands/FlashFill.vi"/>
		<Item Name="InitializeBootLoader.vi" Type="VI" URL="../_NET Bootload Commands/InitializeBootLoader.vi"/>
		<Item Name="JumpToApplication.vi" Type="VI" URL="../_NET Bootload Commands/JumpToApplication.vi"/>
		<Item Name="JumpToBootLoader.vi" Type="VI" URL="../_NET Bootload Commands/JumpToBootLoader.vi"/>
		<Item Name="mPower.dll" Type="Document" URL="../_NET Bootload Commands/mPower.dll"/>
		<Item Name="ParseSRecords.vi" Type="VI" URL="../_NET Bootload Commands/ParseSRecords.vi"/>
		<Item Name="ProgramFlash.vi" Type="VI" URL="../_NET Bootload Commands/ProgramFlash.vi"/>
		<Item Name="UpdateFirmwareApp.vi" Type="VI" URL="../_NET Bootload Commands/UpdateFirmwareApp.vi"/>
	</Item>
	<Item Name="Bootloader Commands" Type="Folder">
		<Item Name="_AppVerify.vi" Type="VI" URL="../Bootloader Commands/_AppVerify.vi"/>
		<Item Name="_ChangeBaud.vi" Type="VI" URL="../Bootloader Commands/_ChangeBaud.vi"/>
		<Item Name="_CheckImageCRC.vi" Type="VI" URL="../Bootloader Commands/_CheckImageCRC.vi"/>
		<Item Name="_EraseFlash.vi" Type="VI" URL="../Bootloader Commands/_EraseFlash.vi"/>
		<Item Name="_FlashFill.vi" Type="VI" URL="../Bootloader Commands/_FlashFill.vi"/>
		<Item Name="_FlashFill_Packet.vi" Type="VI" URL="../Bootloader Commands/_FlashFill_Packet.vi"/>
		<Item Name="_FlashWithAddress_Packet.vi" Type="VI" URL="../Bootloader Commands/_FlashWithAddress_Packet.vi"/>
		<Item Name="_FlashWithOutAddress_Packet.vi" Type="VI" URL="../Bootloader Commands/_FlashWithOutAddress_Packet.vi"/>
		<Item Name="_GetAddressLength.vi" Type="VI" URL="../Bootloader Commands/_GetAddressLength.vi"/>
		<Item Name="_GetBootloaderImageBounderiesvi.vi" Type="VI" URL="../Bootloader Commands/_GetBootloaderImageBounderiesvi.vi"/>
		<Item Name="_GetDataPackets.vi" Type="VI" URL="../Bootloader Commands/_GetDataPackets.vi"/>
		<Item Name="_JumpToApp.vi" Type="VI" URL="../Bootloader Commands/_JumpToApp.vi"/>
		<Item Name="_JumpToBoot.vi" Type="VI" URL="../Bootloader Commands/_JumpToBoot.vi"/>
		<Item Name="_ProgramFlash.vi" Type="VI" URL="../Bootloader Commands/_ProgramFlash.vi"/>
		<Item Name="_SearchSRecordDataTable.vi" Type="VI" URL="../Bootloader Commands/_SearchSRecordDataTable.vi"/>
		<Item Name="_WriteFlash.vi" Type="VI" URL="../Bootloader Commands/_WriteFlash.vi"/>
		<Item Name="UpdateFirmwareApp_New.vi" Type="VI" URL="../Bootloader Commands/UpdateFirmwareApp_New.vi"/>
	</Item>
	<Item Name="Forda Commands" Type="Folder">
		<Item Name="Forda_GetCruiseMode.vi" Type="VI" URL="../Forda Commands/Forda_GetCruiseMode.vi"/>
		<Item Name="Forda_GetCruiseModules.vi" Type="VI" URL="../Forda Commands/Forda_GetCruiseModules.vi"/>
		<Item Name="Forda_GetDeviceType.vi" Type="VI" URL="../Forda Commands/Forda_GetDeviceType.vi"/>
		<Item Name="Forda_GetInputWireState.vi" Type="VI" URL="../Forda Commands/Forda_GetInputWireState.vi"/>
		<Item Name="Forda_GetTakedownModules.vi" Type="VI" URL="../Forda Commands/Forda_GetTakedownModules.vi"/>
		<Item Name="Forda_GetTakedownWorklightMode.vi" Type="VI" URL="../Forda Commands/Forda_GetTakedownWorklightMode.vi"/>
		<Item Name="Forda_GetWorklightModules.vi" Type="VI" URL="../Forda Commands/Forda_GetWorklightModules.vi"/>
		<Item Name="Forda_SetCruiseMode.vi" Type="VI" URL="../Forda Commands/Forda_SetCruiseMode.vi"/>
		<Item Name="Forda_SetCruiseModules.vi" Type="VI" URL="../Forda Commands/Forda_SetCruiseModules.vi"/>
		<Item Name="Forda_SetDeviceType.vi" Type="VI" URL="../Forda Commands/Forda_SetDeviceType.vi"/>
		<Item Name="Forda_SetInputWireState.vi" Type="VI" URL="../Forda Commands/Forda_SetInputWireState.vi"/>
		<Item Name="Forda_SetTakedownModules.vi" Type="VI" URL="../Forda Commands/Forda_SetTakedownModules.vi"/>
		<Item Name="Forda_SetWorklightMode.vi" Type="VI" URL="../Forda Commands/Forda_SetWorklightMode.vi"/>
		<Item Name="Forda_SetWorklightModules.vi" Type="VI" URL="../Forda Commands/Forda_SetWorklightModules.vi"/>
	</Item>
	<Item Name="MESA Commands" Type="Folder">
		<Item Name="MESA_ControlViaLIN.vi" Type="VI" URL="../MESA Commands/MESA_ControlViaLIN.vi"/>
		<Item Name="MESA_GetADValue.vi" Type="VI" URL="../MESA Commands/MESA_GetADValue.vi"/>
		<Item Name="MESA_GetAlleyModules.vi" Type="VI" URL="../MESA Commands/MESA_GetAlleyModules.vi"/>
		<Item Name="MESA_GetArrowMode.vi" Type="VI" URL="../MESA Commands/MESA_GetArrowMode.vi"/>
		<Item Name="MESA_GetColorMode.vi" Type="VI" URL="../MESA Commands/MESA_GetColorMode.vi"/>
		<Item Name="MESA_GetCruiseDutyCycle.vi" Type="VI" URL="../MESA Commands/MESA_GetCruiseDutyCycle.vi"/>
		<Item Name="MESA_GetCruiseMode.vi" Type="VI" URL="../MESA Commands/MESA_GetCruiseMode.vi"/>
		<Item Name="MESA_GetCruiseModules.vi" Type="VI" URL="../MESA Commands/MESA_GetCruiseModules.vi"/>
		<Item Name="MESA_GetDeviceType.vi" Type="VI" URL="../MESA Commands/MESA_GetDeviceType.vi"/>
		<Item Name="MESA_GetDipSwitchSettings.vi" Type="VI" URL="../MESA Commands/MESA_GetDipSwitchSettings.vi"/>
		<Item Name="MESA_GetInputWireState.vi" Type="VI" URL="../MESA Commands/MESA_GetInputWireState.vi"/>
		<Item Name="MESA_GetPhase.vi" Type="VI" URL="../MESA Commands/MESA_GetPhase.vi"/>
		<Item Name="MESA_GetSceneMode.vi" Type="VI" URL="../MESA Commands/MESA_GetSceneMode.vi"/>
		<Item Name="MESA_GetTakedownModules.vi" Type="VI" URL="../MESA Commands/MESA_GetTakedownModules.vi"/>
		<Item Name="MESA_GetWarningArrowPattern.vi" Type="VI" URL="../MESA Commands/MESA_GetWarningArrowPattern.vi"/>
		<Item Name="MESA_GetWorklightModules.vi" Type="VI" URL="../MESA Commands/MESA_GetWorklightModules.vi"/>
		<Item Name="MESA_SetAlleyMode.vi" Type="VI" URL="../MESA Commands/MESA_SetAlleyMode.vi"/>
		<Item Name="MESA_SetArrowMode.vi" Type="VI" URL="../MESA Commands/MESA_SetArrowMode.vi"/>
		<Item Name="MESA_SetColorMode.vi" Type="VI" URL="../MESA Commands/MESA_SetColorMode.vi"/>
		<Item Name="MESA_SetCruiseDutyCycle.vi" Type="VI" URL="../MESA Commands/MESA_SetCruiseDutyCycle.vi"/>
		<Item Name="MESA_SetCruiseMode.vi" Type="VI" URL="../MESA Commands/MESA_SetCruiseMode.vi"/>
		<Item Name="MESA_SetCruiseModules.vi" Type="VI" URL="../MESA Commands/MESA_SetCruiseModules.vi"/>
		<Item Name="MESA_SetDeviceType.vi" Type="VI" URL="../MESA Commands/MESA_SetDeviceType.vi"/>
		<Item Name="MESA_SetInputWire.vi" Type="VI" URL="../MESA Commands/MESA_SetInputWire.vi"/>
		<Item Name="MESA_SetInputWireState.vi" Type="VI" URL="../MESA Commands/MESA_SetInputWireState.vi"/>
		<Item Name="MESA_SetPhaseMode.vi" Type="VI" URL="../MESA Commands/MESA_SetPhaseMode.vi"/>
		<Item Name="MESA_SetSceneMode.vi" Type="VI" URL="../MESA Commands/MESA_SetSceneMode.vi"/>
		<Item Name="MESA_SetTakedownMode.vi" Type="VI" URL="../MESA Commands/MESA_SetTakedownMode.vi"/>
		<Item Name="MESA_SetWarningArrowPattern.vi" Type="VI" URL="../MESA Commands/MESA_SetWarningArrowPattern.vi"/>
		<Item Name="MESA_SetWorklightMode.vi" Type="VI" URL="../MESA Commands/MESA_SetWorklightMode.vi"/>
	</Item>
	<Item Name="mPower Commands" Type="Folder">
		<Item Name="Ford_FactoryDefaults.vi" Type="VI" URL="../mPower Commands/Ford_FactoryDefaults.vi"/>
		<Item Name="Ford_SetTable7.vi" Type="VI" URL="../mPower Commands/Ford_SetTable7.vi"/>
		<Item Name="Ford_SetWireFunction.vi" Type="VI" URL="../mPower Commands/Ford_SetWireFunction.vi"/>
		<Item Name="Get5WireInput.vi" Type="VI" URL="../mPower Commands/Get5WireInput.vi"/>
		<Item Name="GetFlasherSettings.vi" Type="VI" URL="../mPower Commands/GetFlasherSettings.vi"/>
		<Item Name="GetTable4Status.vi" Type="VI" URL="../mPower Commands/GetTable4Status.vi"/>
		<Item Name="mPower3_ActivateLEDBySerialNumber.vi" Type="VI" URL="../mPower Commands/mPower3_ActivateLEDBySerialNumber.vi"/>
		<Item Name="mPower3_BlinkId.vi" Type="VI" URL="../mPower Commands/mPower3_BlinkId.vi"/>
		<Item Name="mPower3_GetALS.vi" Type="VI" URL="../mPower Commands/mPower3_GetALS.vi"/>
		<Item Name="mPower3_GetMaxDutyCycle_Running.vi" Type="VI" URL="../mPower Commands/mPower3_GetMaxDutyCycle_Running.vi"/>
		<Item Name="mPower3_GetMaxDutyCycle_Thermal.vi" Type="VI" URL="../mPower Commands/mPower3_GetMaxDutyCycle_Thermal.vi"/>
		<Item Name="mPower3_GetMaxDutyCyle_Time.vi" Type="VI" URL="../mPower Commands/mPower3_GetMaxDutyCyle_Time.vi"/>
		<Item Name="mPower3_GetTempAdc.vi" Type="VI" URL="../mPower Commands/mPower3_GetTempAdc.vi"/>
		<Item Name="mPower3_GetWdog.vi" Type="VI" URL="../mPower Commands/mPower3_GetWdog.vi"/>
		<Item Name="mPower3_SysReg.vi" Type="VI" URL="../mPower Commands/mPower3_SysReg.vi"/>
		<Item Name="mPower_GetModuleConfiguration.vi" Type="VI" URL="../mPower Commands/mPower_GetModuleConfiguration.vi"/>
		<Item Name="mPower_GetPcbTemp.vi" Type="VI" URL="../mPower Commands/mPower_GetPcbTemp.vi"/>
		<Item Name="ResetmPowerECEDefault.vi" Type="VI" URL="../mPower Commands/ResetmPowerECEDefault.vi"/>
		<Item Name="SearchConnectedDevices.vi" Type="VI" URL="../mPower Commands/SearchConnectedDevices.vi"/>
		<Item Name="SetFlasherSettings.vi" Type="VI" URL="../mPower Commands/SetFlasherSettings.vi"/>
		<Item Name="SetTable4Status.vi" Type="VI" URL="../mPower Commands/SetTable4Status.vi"/>
		<Item Name="TurnOnLedBySN.vi" Type="VI" URL="../mPower Commands/TurnOnLedBySN.vi"/>
	</Item>
	<Item Name="nForce Interior Lightbar" Type="Folder">
		<Item Name="nForceInt_GetArrow.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_GetArrow.vi"/>
		<Item Name="nForceInt_GetDipSwitchSettings.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_GetDipSwitchSettings.vi"/>
		<Item Name="nForceInt_GetInputWireState.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_GetInputWireState.vi"/>
		<Item Name="nForceInt_GetSwapMode.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_GetSwapMode.vi"/>
		<Item Name="nForceInt_GetTakedownMode.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_GetTakedownMode.vi"/>
		<Item Name="nForceInt_SetArrow.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_SetArrow.vi"/>
		<Item Name="nForceInt_SetSwapMode.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_SetSwapMode.vi"/>
		<Item Name="nForceInt_SetTakedownMode.vi" Type="VI" URL="../nForce Interior Lightbar/nForceInt_SetTakedownMode.vi"/>
	</Item>
	<Item Name="nRoads Commands" Type="Folder">
		<Item Name="ControlWireColorDutyCycle.vi" Type="VI" URL="../nRoads Commands/ControlWireColorDutyCycle.vi"/>
		<Item Name="GetAdditionalModuleConfig.vi" Type="VI" URL="../nRoads Commands/GetAdditionalModuleConfig.vi"/>
		<Item Name="GetControlWirePattern.vi" Type="VI" URL="../nRoads Commands/GetControlWirePattern.vi"/>
		<Item Name="GetControlWireStatus.vi" Type="VI" URL="../nRoads Commands/GetControlWireStatus.vi"/>
		<Item Name="GetDipSwitchSettings.vi" Type="VI" URL="../nRoads Commands/GetDipSwitchSettings.vi"/>
		<Item Name="GetModuleConfiguration.vi" Type="VI" URL="../nRoads Commands/GetModuleConfiguration.vi"/>
		<Item Name="GetnRoadsModuleId.vi" Type="VI" URL="../nRoads Commands/GetnRoadsModuleId.vi"/>
		<Item Name="SetnRoadsCntlWire.vi" Type="VI" URL="../nRoads Commands/SetnRoadsCntlWire.vi"/>
		<Item Name="StartLearnFunction.vi" Type="VI" URL="../nRoads Commands/StartLearnFunction.vi"/>
	</Item>
	<Item Name="nRoads Remote" Type="Folder">
		<Item Name="nRoadsRemote_GetFirmware.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_GetFirmware.vi"/>
		<Item Name="nRoadsRemote_GetProgButtonStatus.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_GetProgButtonStatus.vi"/>
		<Item Name="nRoadsRemote_GetSerialNumber.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_GetSerialNumber.vi"/>
		<Item Name="nRoadsRemote_GetSleepMode.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_GetSleepMode.vi"/>
		<Item Name="nRoadsRemote_SetBuzzer.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_SetBuzzer.vi"/>
		<Item Name="nRoadsRemote_SetSerialNumber.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_SetSerialNumber.vi"/>
		<Item Name="nRoadsRemote_SetSleepMode.vi" Type="VI" URL="../nRoads Remote/nRoadsRemote_SetSleepMode.vi"/>
		<Item Name="RemoteStatus.vi" Type="VI" URL="../nRoads Remote/RemoteStatus.vi"/>
	</Item>
	<Item Name="Support VIs" Type="Folder">
		<Item Name="Inline CRC" Type="Folder">
			<Item Name="CRC SubVIs" Type="Folder">
				<Item Name="Wrappers" Type="Folder">
					<Item Name="CRC-8-CCITT.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Wrappers/CRC-8-CCITT.vi"/>
					<Item Name="CRC-8-Maxim 1-Wire (DOW).vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Wrappers/CRC-8-Maxim 1-Wire (DOW).vi"/>
					<Item Name="CRC-16-CCITT-0x1D0F.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Wrappers/CRC-16-CCITT-0x1D0F.vi"/>
					<Item Name="CRC-16-CCITT-0xFFFF.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Wrappers/CRC-16-CCITT-0xFFFF.vi"/>
					<Item Name="CRC-16-CCITT-xMODEM.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Wrappers/CRC-16-CCITT-xMODEM.vi"/>
				</Item>
				<Item Name="CRC State.ctl" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/CRC State.ctl"/>
				<Item Name="dir.mnu" Type="Document" URL="../Support VIs/Inline CRC/CRC SubVIs/dir.mnu"/>
				<Item Name="Inline CRC-8-CCITT &amp; CRC-8-Maxim.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Inline CRC-8-CCITT &amp; CRC-8-Maxim.vi"/>
				<Item Name="Inline CRC-16-CCITT.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Inline CRC-16-CCITT.vi"/>
				<Item Name="Inline CRC-32.vi" Type="VI" URL="../Support VIs/Inline CRC/CRC SubVIs/Inline CRC-32.vi"/>
			</Item>
			<Item Name="dir.mnu" Type="Document" URL="../Support VIs/Inline CRC/dir.mnu"/>
			<Item Name="Example.vi" Type="VI" URL="../Support VIs/Inline CRC/Example.vi"/>
			<Item Name="Inline CRC.vi" Type="VI" URL="../Support VIs/Inline CRC/Inline CRC.vi"/>
		</Item>
		<Item Name="5WireUc_ClusterOfColorId.ctl" Type="VI" URL="../Support VIs/5WireUc_ClusterOfColorId.ctl"/>
		<Item Name="Alley Mode.ctl" Type="VI" URL="../Support VIs/Alley Mode.ctl"/>
		<Item Name="Cluster of DIP.ctl" Type="VI" URL="../Support VIs/Cluster of DIP.ctl"/>
		<Item Name="Cluster of Input Wires.ctl" Type="VI" URL="../Support VIs/Cluster of Input Wires.ctl"/>
		<Item Name="Cluster Of ModInfo.ctl" Type="VI" URL="../Support VIs/Cluster Of ModInfo.ctl"/>
		<Item Name="Cluster of TD Mode Control.ctl" Type="VI" URL="../Support VIs/Cluster of TD Mode Control.ctl"/>
		<Item Name="Cluster of WL Mode Control.ctl" Type="VI" URL="../Support VIs/Cluster of WL Mode Control.ctl"/>
		<Item Name="Cluster_5Wire_FlasherSetting.ctl" Type="VI" URL="../Support VIs/Cluster_5Wire_FlasherSetting.ctl"/>
		<Item Name="Cluster_5Wire_WireState.ctl" Type="VI" URL="../Support VIs/Cluster_5Wire_WireState.ctl"/>
		<Item Name="Cluster_ControllerInfo.ctl" Type="VI" URL="../Support VIs/Cluster_ControllerInfo.ctl"/>
		<Item Name="Cluster_Eeprom.ctl" Type="VI" URL="../Support VIs/Cluster_Eeprom.ctl"/>
		<Item Name="Cluster_Forda_Input Wires.ctl" Type="VI" URL="../Support VIs/Cluster_Forda_Input Wires.ctl"/>
		<Item Name="Cluster_Functions.ctl" Type="VI" URL="../Support VIs/Cluster_Functions.ctl"/>
		<Item Name="Cluster_MESA_AdReading.ctl" Type="VI" URL="../Support VIs/Cluster_MESA_AdReading.ctl"/>
		<Item Name="Cluster_MESA_ColorMode.ctl" Type="VI" URL="../Support VIs/Cluster_MESA_ColorMode.ctl"/>
		<Item Name="Cluster_mPower3_Watchdog.ctl" Type="VI" URL="../Support VIs/Cluster_mPower3_Watchdog.ctl"/>
		<Item Name="Cluster_nRoads_AdditionalModuleConfig.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_AdditionalModuleConfig.ctl"/>
		<Item Name="Cluster_nRoads_ControlWire.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_ControlWire.ctl"/>
		<Item Name="Cluster_nRoads_ControlWireState.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_ControlWireState.ctl"/>
		<Item Name="Cluster_nRoads_DIPSW.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_DIPSW.ctl"/>
		<Item Name="Cluster_nRoads_Module1.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module1.ctl"/>
		<Item Name="Cluster_nRoads_Module2.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module2.ctl"/>
		<Item Name="Cluster_nRoads_Module3.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module3.ctl"/>
		<Item Name="Cluster_nRoads_Module4.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module4.ctl"/>
		<Item Name="Cluster_nRoads_Module5.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module5.ctl"/>
		<Item Name="Cluster_nRoads_Module6.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module6.ctl"/>
		<Item Name="Cluster_nRoads_Module7.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module7.ctl"/>
		<Item Name="Cluster_nRoads_Module8.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module8.ctl"/>
		<Item Name="Cluster_nRoads_Module9.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_Module9.ctl"/>
		<Item Name="Cluster_nRoads_ModuleColor_DutyCycle.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_ModuleColor_DutyCycle.ctl"/>
		<Item Name="Cluster_nRoads_ModuleID1_9.ctl" Type="VI" URL="../Support VIs/Cluster_nRoads_ModuleID1_9.ctl"/>
		<Item Name="Cluster_Temp.ctl" Type="VI" URL="../Support VIs/Cluster_Temp.ctl"/>
		<Item Name="Cluster_Voltage.ctl" Type="VI" URL="../Support VIs/Cluster_Voltage.ctl"/>
		<Item Name="ClusterOfCruiseModules.ctl" Type="VI" URL="../Support VIs/ClusterOfCruiseModules.ctl"/>
		<Item Name="Command_ModeID.vi" Type="VI" URL="../Support VIs/Command_ModeID.vi"/>
		<Item Name="Compare CRC_Sub_VI.vi" Type="VI" URL="../Support VIs/Compare CRC_Sub_VI.vi"/>
		<Item Name="CRC SubVI.vi" Type="VI" URL="../Support VIs/CRC SubVI.vi"/>
		<Item Name="CruiseModeModules.ctl" Type="VI" URL="../Support VIs/CruiseModeModules.ctl"/>
		<Item Name="Ford_WireSettings.ctl" Type="VI" URL="../Support VIs/Ford_WireSettings.ctl"/>
		<Item Name="Forda_TypeDef_CruiseModules.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_CruiseModules.ctl"/>
		<Item Name="Forda_TypeDef_DeviceType.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_DeviceType.ctl"/>
		<Item Name="Forda_TypeDef_FunctionCodeWarning.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_FunctionCodeWarning.ctl"/>
		<Item Name="Forda_TypeDef_TakedownMode.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_TakedownMode.ctl"/>
		<Item Name="Forda_TypeDef_TakedownModules.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_TakedownModules.ctl"/>
		<Item Name="Forda_TypeDef_TD_WLmode.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_TD_WLmode.ctl"/>
		<Item Name="Forda_TypeDef_WorklightModules.ctl" Type="VI" URL="../Support VIs/Forda_TypeDef_WorklightModules.ctl"/>
		<Item Name="Function Code_WarningArrow.ctl" Type="VI" URL="../Support VIs/Function Code_WarningArrow.ctl"/>
		<Item Name="LED Config Decode.vi" Type="VI" URL="../Support VIs/LED Config Decode.vi"/>
		<Item Name="LIN_CrcMatch.vi" Type="VI" URL="../Support VIs/LIN_CrcMatch.vi"/>
		<Item Name="LIN_Mod ID_Sub_VI.vi" Type="VI" URL="../Support VIs/LIN_Mod ID_Sub_VI.vi"/>
		<Item Name="LIN_SerialReadWrite.vi" Type="VI" URL="../Support VIs/LIN_SerialReadWrite.vi"/>
		<Item Name="Module Info Cluster.ctl" Type="VI" URL="../Support VIs/Module Info Cluster.ctl"/>
		<Item Name="nForceInt_TD_Enum.ctl" Type="VI" URL="../Support VIs/nForceInt_TD_Enum.ctl"/>
		<Item Name="Product Code_TypeDef.ctl" Type="VI" URL="../Support VIs/Product Code_TypeDef.ctl"/>
		<Item Name="Product Type_Sub_VI.vi" Type="VI" URL="../Support VIs/Product Type_Sub_VI.vi"/>
		<Item Name="ProductTypeToProductID.vi" Type="VI" URL="../Support VIs/ProductTypeToProductID.vi"/>
		<Item Name="SceneMode.ctl" Type="VI" URL="../Support VIs/SceneMode.ctl"/>
		<Item Name="Strip Away CRC_Sub_VI.vi" Type="VI" URL="../Support VIs/Strip Away CRC_Sub_VI.vi"/>
		<Item Name="TD_SW5_Off.ctl" Type="VI" URL="../Support VIs/TD_SW5_Off.ctl"/>
		<Item Name="TD_SW5_On.ctl" Type="VI" URL="../Support VIs/TD_SW5_On.ctl"/>
		<Item Name="TypeDef_ADChannel.ctl" Type="VI" URL="../Support VIs/TypeDef_ADChannel.ctl"/>
		<Item Name="TypeDef_ArrowMode.ctl" Type="VI" URL="../Support VIs/TypeDef_ArrowMode.ctl"/>
		<Item Name="TypeDef_CruiseDutyCycle.ctl" Type="VI" URL="../Support VIs/TypeDef_CruiseDutyCycle.ctl"/>
		<Item Name="TypeDef_CruiseMode.ctl" Type="VI" URL="../Support VIs/TypeDef_CruiseMode.ctl"/>
		<Item Name="TypeDef_CruiseModulesSw4Off.ctl" Type="VI" URL="../Support VIs/TypeDef_CruiseModulesSw4Off.ctl"/>
		<Item Name="TypeDef_FunctionCodeArrow.ctl" Type="VI" URL="../Support VIs/TypeDef_FunctionCodeArrow.ctl"/>
		<Item Name="TypeDef_FunctionCodeWarning.ctl" Type="VI" URL="../Support VIs/TypeDef_FunctionCodeWarning.ctl"/>
		<Item Name="TypeDef_FunctionNumber.ctl" Type="VI" URL="../Support VIs/TypeDef_FunctionNumber.ctl"/>
		<Item Name="TypeDef_FunctionPhaseArrow.ctl" Type="VI" URL="../Support VIs/TypeDef_FunctionPhaseArrow.ctl"/>
		<Item Name="TypeDef_FunctionPhaseWarning.ctl" Type="VI" URL="../Support VIs/TypeDef_FunctionPhaseWarning.ctl"/>
		<Item Name="TypeDef_LedColor.ctl" Type="VI" URL="../Support VIs/TypeDef_LedColor.ctl"/>
		<Item Name="TypeDef_LedConfiguration.ctl" Type="VI" URL="../Support VIs/TypeDef_LedConfiguration.ctl"/>
		<Item Name="TypeDef_SW4_TD.ctl" Type="VI" URL="../Support VIs/TypeDef_SW4_TD.ctl"/>
		<Item Name="TypeDef_SW4_TD_WL.ctl" Type="VI" URL="../Support VIs/TypeDef_SW4_TD_WL.ctl"/>
		<Item Name="TypeDef_WorklightModeSw5Off.ctl" Type="VI" URL="../Support VIs/TypeDef_WorklightModeSw5Off.ctl"/>
		<Item Name="WaitDialogBox.vi" Type="VI" URL="../Support VIs/WaitDialogBox.vi"/>
		<Item Name="Worklight Mode.ctl" Type="VI" URL="../Support VIs/Worklight Mode.ctl"/>
	</Item>
	<Item Name="Unicor Commands" Type="Folder"/>
	<Item Name="ControlViaLIN.vi" Type="VI" URL="../ControlViaLIN.vi"/>
	<Item Name="FactoryReset.vi" Type="VI" URL="../FactoryReset.vi"/>
	<Item Name="GetFirmwareID.vi" Type="VI" URL="../GetFirmwareID.vi"/>
	<Item Name="GetSerialNumber.vi" Type="VI" URL="../GetSerialNumber.vi"/>
	<Item Name="GetSystemVoltage.vi" Type="VI" URL="../GetSystemVoltage.vi"/>
	<Item Name="InitCommPort.vi" Type="VI" URL="../InitCommPort.vi"/>
	<Item Name="LIN_CloseCommPort.vi" Type="VI" URL="../LIN_CloseCommPort.vi"/>
	<Item Name="ReadEepromMemory.vi" Type="VI" URL="../ReadEepromMemory.vi"/>
	<Item Name="SetImmediateShutDown.vi" Type="VI" URL="../SetImmediateShutDown.vi"/>
	<Item Name="SetModId.vi" Type="VI" URL="../SetModId.vi"/>
	<Item Name="SetModIdBySn.vi" Type="VI" URL="../SetModIdBySn.vi"/>
	<Item Name="WriteSerialNumber.vi" Type="VI" URL="../WriteSerialNumber.vi"/>
</Library>
