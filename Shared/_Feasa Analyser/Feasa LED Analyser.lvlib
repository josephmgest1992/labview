﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Instance VIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="BusBusy.vi" Type="VI" URL="../Instance VIs/BusBusy.vi"/>
		<Item Name="BusCapture.vi" Type="VI" URL="../Instance VIs/BusCapture.vi"/>
		<Item Name="BusFree.vi" Type="VI" URL="../Instance VIs/BusFree.vi"/>
		<Item Name="BusGet.vi" Type="VI" URL="../Instance VIs/BusGet.vi"/>
		<Item Name="Capture-Normal.vi" Type="VI" URL="../Instance VIs/Capture-Normal.vi"/>
		<Item Name="Capture-PWM.vi" Type="VI" URL="../Instance VIs/Capture-PWM.vi"/>
		<Item Name="Get7Seg.vi" Type="VI" URL="../Instance VIs/Get7Seg.vi"/>
		<Item Name="GetAbsIntensity.vi" Type="VI" URL="../Instance VIs/GetAbsIntensity.vi"/>
		<Item Name="GetAbsIntensityMult.vi" Type="VI" URL="../Instance VIs/GetAbsIntensityMult.vi"/>
		<Item Name="GetCCT-all.vi" Type="VI" URL="../Instance VIs/GetCCT-all.vi"/>
		<Item Name="GetCCT.vi" Type="VI" URL="../Instance VIs/GetCCT.vi"/>
		<Item Name="GetExposureFactor.vi" Type="VI" URL="../Instance VIs/GetExposureFactor.vi"/>
		<Item Name="GetHSI-all.vi" Type="VI" URL="../Instance VIs/GetHSI-all.vi"/>
		<Item Name="GetHSI.vi" Type="VI" URL="../Instance VIs/GetHSI.vi"/>
		<Item Name="GetIntensity-all.vi" Type="VI" URL="../Instance VIs/GetIntensity-all.vi"/>
		<Item Name="GetIntensity.vi" Type="VI" URL="../Instance VIs/GetIntensity.vi"/>
		<Item Name="GetIntensityGain.vi" Type="VI" URL="../Instance VIs/GetIntensityGain.vi"/>
		<Item Name="GetRGBI-all.vi" Type="VI" URL="../Instance VIs/GetRGBI-all.vi"/>
		<Item Name="GetRGBI.vi" Type="VI" URL="../Instance VIs/GetRGBI.vi"/>
		<Item Name="GetUV-all.vi" Type="VI" URL="../Instance VIs/GetUV-all.vi"/>
		<Item Name="GetUV.vi" Type="VI" URL="../Instance VIs/GetUV.vi"/>
		<Item Name="GetWavelength-all.vi" Type="VI" URL="../Instance VIs/GetWavelength-all.vi"/>
		<Item Name="GetWavelength.vi" Type="VI" URL="../Instance VIs/GetWavelength.vi"/>
		<Item Name="GetWavelengthOffset.vi" Type="VI" URL="../Instance VIs/GetWavelengthOffset.vi"/>
		<Item Name="GetWI-all.vi" Type="VI" URL="../Instance VIs/GetWI-all.vi"/>
		<Item Name="GetWI.vi" Type="VI" URL="../Instance VIs/GetWI.vi"/>
		<Item Name="GetXoffset.vi" Type="VI" URL="../Instance VIs/GetXoffset.vi"/>
		<Item Name="GetXY-all.vi" Type="VI" URL="../Instance VIs/GetXY-all.vi"/>
		<Item Name="GetXY.vi" Type="VI" URL="../Instance VIs/GetXY.vi"/>
		<Item Name="GetYoffset.vi" Type="VI" URL="../Instance VIs/GetYoffset.vi"/>
		<Item Name="SetAbsIntensityMult.vi" Type="VI" URL="../Instance VIs/SetAbsIntensityMult.vi"/>
		<Item Name="SetExposureFactor.vi" Type="VI" URL="../Instance VIs/SetExposureFactor.vi"/>
		<Item Name="SetIntensityGain.vi" Type="VI" URL="../Instance VIs/SetIntensityGain.vi"/>
		<Item Name="SetScale.vi" Type="VI" URL="../Instance VIs/SetScale.vi"/>
		<Item Name="SetWavelengthOffset.vi" Type="VI" URL="../Instance VIs/SetWavelengthOffset.vi"/>
		<Item Name="SetXoffset.vi" Type="VI" URL="../Instance VIs/SetXoffset.vi"/>
		<Item Name="SetYoffset.vi" Type="VI" URL="../Instance VIs/SetYoffset.vi"/>
	</Item>
	<Item Name="Sub-VIs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="FormatDbl.vi" Type="VI" URL="../Sub-VIs/FormatDbl.vi"/>
		<Item Name="OK.vi" Type="VI" URL="../Sub-VIs/OK.vi"/>
		<Item Name="RangeFactor.vi" Type="VI" URL="../Sub-VIs/RangeFactor.vi"/>
		<Item Name="RGBI-U32.vi" Type="VI" URL="../Sub-VIs/RGBI-U32.vi"/>
		<Item Name="Wr-Rd (multi).vi" Type="VI" URL="../Sub-VIs/Wr-Rd (multi).vi"/>
		<Item Name="Wr-Rd.vi" Type="VI" URL="../Sub-VIs/Wr-Rd.vi"/>
	</Item>
	<Item Name="Typedef" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="CaptureSettings.ctl" Type="VI" URL="../Typedef/CaptureSettings.ctl"/>
		<Item Name="CCT.ctl" Type="VI" URL="../Typedef/CCT.ctl"/>
		<Item Name="ConvertXY_FGV.ctl" Type="VI" URL="../Typedef/ConvertXY_FGV.ctl"/>
		<Item Name="ConvertXYState.ctl" Type="VI" URL="../Typedef/ConvertXYState.ctl"/>
		<Item Name="HSI.ctl" Type="VI" URL="../Typedef/HSI.ctl"/>
		<Item Name="Range.ctl" Type="VI" URL="../Typedef/Range.ctl"/>
		<Item Name="RGBI.ctl" Type="VI" URL="../Typedef/RGBI.ctl"/>
		<Item Name="Scale.ctl" Type="VI" URL="../Typedef/Scale.ctl"/>
		<Item Name="UVChromacity.ctl" Type="VI" URL="../Typedef/UVChromacity.ctl"/>
		<Item Name="WI.ctl" Type="VI" URL="../Typedef/WI.ctl"/>
		<Item Name="XYChromacity.ctl" Type="VI" URL="../Typedef/XYChromacity.ctl"/>
	</Item>
	<Item Name="Bus Command.vi" Type="VI" URL="../Bus Command.vi"/>
	<Item Name="Capture.vi" Type="VI" URL="../Capture.vi"/>
	<Item Name="Config Port.vi" Type="VI" URL="../Config Port.vi"/>
	<Item Name="Convert Intensity to 1 or 0 .vi" Type="VI" URL="../Convert Intensity to 1 or 0 .vi"/>
	<Item Name="Convert XY to Color Letter.vi" Type="VI" URL="../Convert XY to Color Letter.vi"/>
	<Item Name="Get Data(all).vi" Type="VI" URL="../Get Data(all).vi"/>
	<Item Name="Get Data.vi" Type="VI" URL="../Get Data.vi"/>
	<Item Name="Get Property.vi" Type="VI" URL="../Get Property.vi"/>
	<Item Name="Load Feasa Offsets.vi" Type="VI" URL="../Load Feasa Offsets.vi"/>
	<Item Name="ReadFeasa_DaisyChainMode.vi" Type="VI" URL="../ReadFeasa_DaisyChainMode.vi"/>
	<Item Name="Set Property.vi" Type="VI" URL="../Set Property.vi"/>
</Library>
